// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	RotateToCursor();
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(TDSMappingContext, 0);
		}
	}
	
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &ATDSCharacter::HandleMove);
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Triggered, this, &ATDSCharacter::HandleAim);
		//EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Triggered, this, &ATDSCharacter::HandleSprint);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Triggered, this, &ATDSCharacter::HandleWalk);

		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Completed, this, &ATDSCharacter::HandleAim);
		//EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ATDSCharacter::HandleSprint);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Completed, this, &ATDSCharacter::HandleWalk);
		
	}
}

//WASD movement
void ATDSCharacter::HandleMove(const FInputActionValue& Value)

{
	FVector2D MovementDirection = Value.Get<FVector2D>();

	FRotator Rotation = Controller->GetControlRotation();
	FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

	FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(ForwardDirection, MovementDirection.Y);

	FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(RightDirection, MovementDirection.X);
}

//Rotate char to cursor
void ATDSCharacter::RotateToCursor()
{
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult HitResult;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);
		
		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.f, FindRotatorResultYaw, 0.f)));
	}
}

//aim right click speed change
void ATDSCharacter::HandleAim(const FInputActionValue& Value)
{
	AimTriggered = Value.Get<bool>();
	HandleAimWalk();
}


//sprint left shift speed change
void ATDSCharacter::HandleSprint()
{
	//SprintTriggered = Value.Get<bool>();
	//if (SprintTriggered && AreVectorsInErrorRange)
	//{
		MovementState = EMovementState::Sprint_State;
		CharacterUpdate();
	//}
	//else 
	//{
	//	HandleDefaultSpeed();
	//}
}


//walk left alt speed change
void ATDSCharacter::HandleWalk(const FInputActionValue& Value)
{
	WalkTriggered = Value.Get<bool>();
	HandleAimWalk();
}


//handle combinations of walking and aiming
void ATDSCharacter::HandleAimWalk()
{
	if (SprintTriggered)
	{

	}
	else if (AimTriggered && WalkTriggered)
	{
		//if walk and aim at the same time
		MovementState = EMovementState::AimWalk_State;
	}
	else if (AimTriggered && !WalkTriggered)
	{
		MovementState = EMovementState::Aim_State;
	}
	else if (!AimTriggered && WalkTriggered)
	{
		MovementState = EMovementState::Walk_State;
	}
	else {
		HandleDefaultSpeed();
	}
	
	CharacterUpdate();
}

//default speed
void ATDSCharacter::HandleDefaultSpeed()
{
	MovementState = EMovementState::Run_State;
	CharacterUpdate();
}

//updaiting char's speed
void ATDSCharacter::CharacterUpdate()
{
	float ResultSpeed;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResultSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResultSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

//void ATDSCharacter::ChangeMovementState(EMovementState NewMovementState)
//{
//	MovementState = NewMovementState;
//	CharacterUpdate();
//}

